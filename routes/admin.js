const express = require('express');
const adminController = require('../controllers/admin')

const router = express.Router();

router.post('/edit-product', adminController.postEditProduct);

router.post('/add-product', adminController.postAddProduct);

router.post('/delete-product', adminController.postDeleteProduct);

router.post('/getSubtypeByType', adminController.getSubtypeByType);

router.post('/getSizeByTypeAndSubtype', adminController.getSizeByTypeAndSubtype);

router.post('/getColorByTypeSubtypeAndSize', adminController.getColorByTypeSubtypeAndSize);

router.post('/getProductBySerial', adminController.getProductBySerial);


module.exports = router;
