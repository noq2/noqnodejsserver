const express = require('express');
const adminController = require('../controllers/admin')
const productController = require('../controllers/product')
const shopController = require('../controllers/shop')

const router = express.Router();

router.post('/purchase-edit-product', productController.postPurchaseEditProduct);

router.post('/check-product-availability', productController.postCheckProductAvailability);

router.post('/checkout', shopController.postCheckout);

module.exports = router;