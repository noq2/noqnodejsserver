let Product = require("../models/product");

/*
 this function getting product parameters, searching fot existing product
 and edit the existing product with the new values
 */
exports.postEditProduct = (req, res, next) => {
    // console.log("edit product...")
    const {type, subType, size, color, price, productSerialNumber, title, image, quantity} = req.body;

    Product.findOne({'productSerialNumber': productSerialNumber})
        .then(product => {
            product.type = type;
            product.subType = subType;
            product.size = size;
            product.color = color;
            product.price = price;
            product.productSerialNumber = productSerialNumber;
            product.title = title;
            product.image = image;
            product.quantity = quantity;

            product.save()
                .then(() => res.json(product))
                .catch(err => res.status(400).json('Error: ' + err))
        })
        .catch(err => res.status(400).json('Error: ' + err))
}

/*
 this function getting product parameters, and add a new product
 with those parameters
 */
exports.postAddProduct = async (req, res, next) => {
    // console.log("add product...")
    const {type, subType, size, color, price, productSerialNumber, title, image, quantity} = req.body;

    console.log({type, subType, size, color, price, productSerialNumber, title, image, quantity})
    let product = await Product.findOne({'productSerialNumber': productSerialNumber});

    if (product) {
        return res.status(400).json(req.body).send();
    }

    let newProduct = new Product({
        type,
        subType,
        size,
        color,
        price,
        productSerialNumber,
        title,
        image,
        quantity
    });
    // saving the new product
    await newProduct.save()
        .then(() => res.status(200).json(newProduct).send())
        .catch(err => {
            return res.status(400).json('Error: ' + err).send()
        })
}

/*
 this function getting product parameters, and searching for existing product
 if product found it been deleted him from the DB
 */
exports.postDeleteProduct = (req, res, next) => {
    console.log("delete product...")
    Product.findOneAndDelete({productSerialNumber: req.body.productSerialNumber})
        .then(product => {
            if (product) {
                // console.log('product deleted');
                return res.status(200).json(req.body).send();
            } else {
                // console.log('product not found');
                return res.status(401).json(req.body).send();
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json(req.body).send();
        })
}

/*
 this function getting product type and searching for all product
 matches for the type and return those products subType
 */
exports.getSubtypeByType = (req, res, next) => {
    Product.find({type: req.body.type})
        .then(products => {
            let subTypes = new Set()
            subTypes.add("Sub Type")
            products.forEach(product => subTypes.add(product.subType))
            let data = arrayToList(Array.from(subTypes));

            console.log('return all products with type ' + req.body.type);
            res.status(200).json(data).send();
            console.log('data' + data)
        })
        .catch(err => {
            console.log(err)
            return res.status(401).json(req.body).send();
        })
}

/*
 this function getting product type and subType, and searching for all product
 matches for the type and subType return those products colors
 */
exports.getColorByTypeSubtypeAndSize = (req, res, next) => {

    Product.find({type: req.body.type, subType: req.body.subType, size: req.body.size})
        .then(products => {
            let colors = new Set()
            colors.add("Color")
            products.forEach(product => colors.add(product.color))
            let data = arrayToList(Array.from(colors));

            console.log('return all products colors with type:' + req.body.type + ' and subType:' + req.body.subType + ' and size:' + req.body.size);
            res.status(200).json(data).send();
            console.log('data' + data)
        })
        .catch(err => {
            console.log(err)
            return res.status(401).json(req.body).send();
        })
}


/*
 this function getting product type, subType, and searching for all product
 matches for the type and subType return those products sizes
 */
exports.getSizeByTypeAndSubtype = (req, res, next) => {
    Product.find({type: req.body.type, subType: req.body.subType})
        .then(products => {
            let sizes = new Set()
            sizes.add("Size")
            products.forEach(product => sizes.add(product.size))
            //sort the sizes arrays
            sizes = Array.from(sizes)
            const sorted_sizes = ["Size", "s", "S", "m", "M", "l", "L", "xl", "XL", "xxl", "XXL", "xxxl", "XXXL"]
            //sorting the array by defined array
            sizes.sort(function (a, b) {
                a = sorted_sizes.indexOf(a)
                b = sorted_sizes.indexOf(b)
                if (b > a) {
                    return -1
                } else {
                    return 1
                }
            });
            let data = arrayToList(sizes);

            // console.log('return all products size with type:' + req.body.type + ' and subType:' +
            //     req.body.subType);
            res.status(200).json(data).send();
            // console.log('data ' + data)
        })
        .catch(err => {
            console.log(err)
            return res.status(401).json(req.body).send();
        })
}

/*
    this function is searching for a product by serial number and returning
    the product that found in the DB
 */
exports.getProductBySerial = (req, res, next) => {
    console.log("getProductBySerial...")
    Product.findOne({productSerialNumber: req.body.productSerialNumber})
        .then(product => {
            if (product) {
                console.log('product found');
                return res.status(200).json(product).send();
            } else {
                console.log('product not found');
                return res.status(401).json(req.body).send();
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json(req.body).send();
        })
}

/*
 This function convert array to list
 */
function arrayToList(array) {
    let list = []
    array.forEach(item => {
        list.push(item)
    });
    return list;
}
