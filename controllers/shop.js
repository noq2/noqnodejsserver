
/*
 mock up function to "accept" payment
 */
exports.postCheckout = (req, res, next) => {
    return res.status(200).json(req.body).send();
}