let Product = require("../models/product");

/*
    this function used to update product quantity in the DB after purchase
 */
exports.postPurchaseEditProduct = async (req, res, next) => {
    // console.log("Purchase edit...")
    let retVal = [];
    for (const item of req.body) {
        const {productSerialNumber, purchaseCounter} = item;

        let product = await Product.findOne({'productSerialNumber': productSerialNumber})

        if (!product) {
            // console.log("Invalid product...")
        }

        if (product.quantity - purchaseCounter < 0) {
            // console.log("Invalid amount...")
            return res.status(401).json(item).send()
        }

        product.quantity = product.quantity - purchaseCounter;
        retVal.push(product)

        product.save()
            .then(() => {
                // console.log("Save new product details...")
            })
            .catch(err => {
                return res.status(400).json('Error: ' + err).send();
            })
    }
    res.json(retVal);

}

/*
    this function used before item added to the shopping basket to check
    if the product is available to purchase.
 */
exports.postCheckProductAvailability = async (req, res, next) => {
    const {type, subType, size, color, price, productSerialNumber, title, img, purchaseCounter} = req.body;
    console.log("Check availability...")

    let retVal = [];
    for (const item of req.body) {
        const {productSerialNumber, purchaseCounter} = item;

        let product = await Product.findOne({'productSerialNumber': productSerialNumber})

        if (!product) {
            console.log("Invalid product...")
            return res.status(401).json(item).send()
        }

        if (product.quantity - purchaseCounter < 0) {
            console.log("Invalid amount...")
            return res.status(401).json(item).send()
        }

        product.quantity = product.quantity - purchaseCounter;
        retVal.push(product)
    }
    console.log("Products available...")
    res.json(retVal);
}