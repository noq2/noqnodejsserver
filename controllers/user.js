let User = require("../models/user");
const bcrypt = require('bcryptjs')
const dotenv = require('dotenv')

dotenv.config(({path: '../config/config.env'}))

/*
 This function used to get all user from the DB
 */
exports.getAllUsers = (req, res) => {
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err))
}

/*
    This function is getting user info parameters and creating new user object
    anf adding the new user to the DB users collection
 */
exports.postAddUser = async (req, res) => {
    const {first_name, last_name, email, password, isAdmin} = req.body;

    //checking first if user already exist
    let user = await User.findOne({email})

    if (user) {
        return res.status(400).json(req.body).send();
    }

    let salt = process.env.salt
    //hashing the user password before saving into the user object
    const hashedPsw = await bcrypt.hash(password, salt)

    let newUser = new User({
        first_name,
        last_name,
        email,
        password: hashedPsw,
        isAdmin: isAdmin? isAdmin:false
    });

    //saving new user to the DB
    await newUser.save()
        .then(() => res.json(newUser))
        .catch(err => {return res.status(400).json('Error: ' + err).send()})

    return res.status(200).send();
}

/*
    This function is searching for existing user by his email, and if user is
    found returning the object info.
 */
exports.getUser = (req, res) => {
    User.findOne({'email': req.body.email})
        .then(user => {
            //is user not exist
            if(!user) {
                return res.status(404).json(req.body).send()
            }
            res.json(user)})
        .catch(err => res.status(400).json('Error: ' + err))
}

/*
    This function is searching for existing user by his email, and if user is
    found is update the user by the new parameters
 */
exports.updateUser =  (req, res) => {
    User.findOne({'email': req.body.email})
        .then(user => {
            if(!user) {
                return res.status(404).json(req.body).send()
            }
            user.first_name = req.body.first_name;
            user.last_name = req.body.last_name;
            user.email = req.body.email;

            user.save()
                .then(() => res.json(user))
                .catch(err => res.status(400).json('Error: ' + err))
        })
        .catch(err => res.status(400).json('Error: ' + err))
}

/*
    This function is searching for existing user by his email, and if user is
    found it been deleted from the DB
 */
exports.deleteUser = (req, res) => {
    User.findOneAndDelete(req.params.email)
        .then(user => res.status(200).json(req.body).send())
        .catch(err => res.status(400).json('Error: ' + err))
}

/*
    This function used to the login process, comparing the information to user from DB, if
    exist and password is matched then log in is approved.
 */
exports.userLogin = async (req, res) => {
    // console.log("log in process")
    const {email, password} = req.body;

    let user = await User.findOne({'email': email});

    if (!user) {
        return res.status(404).json(req.body).send();
    }
    //check if password matchs
    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        // console.log("wrong password")
        return res.status(403).json(req.body).send();
    }

    // console.log("logged in")

    return res.status(200).json(user).send();
}
