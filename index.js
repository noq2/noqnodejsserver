const express = require('express');
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const connectDB = require('./config/db')
const morgan = require('morgan')
const session = require('express-session')
const mongoDBSession = require('connect-mongodb-session')(session);

//load config
dotenv.config(({path: './config/config.env'}))

connectDB();

const app = express();

if (process.env.NODE_ENV === 'dev') {
    app.use(morgan('dev'))
}

//body parser middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

//adding sessions to mongo db
const store = mongoDBSession({
    uri: process.env.MONGO_URI,
    collection: "mySessions",
})

// Express session Middleware
app.use(session({
    secret: 'secret key',
    resave: false,
    saveUninitialized: false,
    // cookie: {secure: true}
    store
}))


const user = require('./routes/user')
app.use('/user', user);

const product = require('./routes/product')
app.use('', product);

const admin = require('./routes/admin')
app.use('', admin);

const PORT = process.env.port || 8000

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}...`)
})