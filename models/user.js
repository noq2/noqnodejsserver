const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create User Schema & model
const UserSchema = new Schema({
    password: {
        type: String,
        required: [true, 'password field is required'],
        minlength: 7
    },
    first_name: {
        type: String,
        required: [true, 'first name field is required'],
        trim: true
    },
    last_name: {
        type: String,
        required: [true, 'last_name field is required'],
        trim: true
    },
    email: {
        type: String,
        required: [true, 'email field is required'],
        unique: true,
        trim: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;

