const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create Item Schema & model
const ProductSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    price: {
        type: Number,
        require: true
    },
    type: { // Shirt, pants
        type: String,
        required: [true, 'Type field is required']
    },
    subType: {// T-shirt, Jeans
        type: String,
        required: [true, 'SubType field is required']
    },
    size: {// TODO: maybe string for S-M-L-XL
        type: String,
        required: [true, 'size field is required']
    },
    color: {//black, white..
        type: String,
        required: [true, 'color field is required']
    },
    quantity: {
        type: Number,
        required: [true, 'color field is required'],
        default: 0
    },
    image: {
        type: String
    },
    productSerialNumber: {
        type: String,
        required: [true, 'productSerialNumber field is required'],
        unique: true,
        trim: true

    }
});

module.exports = mongoose.model('Product', ProductSchema);



